# productionapp-api-proxy-app

This is a NGINX proxy application for my REST application

## Usage

### Environment Variables

* `LISTEN_PORT` - Port to listen on. default: `8000`
* `APP_HOST` - APP Hostname to forward the requests to. default: `app`
* `APP_PORT` - APP Port number to forward the requests to. default : `9000`    
